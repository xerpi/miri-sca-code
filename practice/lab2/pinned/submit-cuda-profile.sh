#!/bin/bash
### Directivas para el gestor de colas
# Asegurar que el job se ejecuta en el directorio actual
#$ -cwd
# Asegurar que el job mantiene las variables de entorno del shell lamador
#$ -V
# Cambiar el nombre del job
#$ -N mxm_profile
# Cambiar el shell
#$ -S /bin/bash


export PROG=mxm.exe
export size=2048
export check=N

export CUDAVER=9.0.176
export PATH=/Soft/cuda/$CUDAVER/bin:$PATH
/Soft/cuda/$CUDAVER/bin/nvprof --cpu-profiling on --unified-memory-profiling off ./$PROG $size $check
