#!/bin/bash
### Directivas para el gestor de colas
# Asegurar que el job se ejecuta en el directorio actual
#$ -cwd
# Asegurar que el job mantiene las variables de entorno del shell lamador
#$ -V
# Cambiar el nombre del job
#$ -N multi-gpu
# Cambiar el shell
#$ -S /bin/bash

# To test the program don't use very large matrices
# N = 512 is enoguth
export CUDAVER=9.0.176
export PATH=/Soft/cuda/$CUDAVER/bin:$PATH
nvprof --cpu-profiling on --unified-memory-profiling off ./kernel4GPUs.exe 512 Y


# With very big matrices it is better not using test
echo "Profiling...."
nvprof --cpu-profiling on --unified-memory-profiling off ./kernel4GPUs.exe 2048 N
nvprof --cpu-profiling on --unified-memory-profiling off ./kernel4GPUs.exe 4096 N
nvprof --cpu-profiling on --unified-memory-profiling off ./kernel4GPUs.exe 8192 N

