#include <stdio.h>
#include <stdlib.h>

#ifndef SIZE
#define SIZE 32
#endif

// Kernel Matriz por Matriz
// C(NxM) <- A(NxP) * B (PxM)

__global__ void KernelMM(int N, int M, int P, float *A, float *B, float *C) {

  __shared__ float sA[SIZE][SIZE];
  __shared__ float sB[SIZE][SIZE];

  int bx = blockIdx.x;  int by = blockIdx.y;
  int tx = threadIdx.x; int ty = threadIdx.y;
  int row = by * SIZE + ty;
  int col = bx * SIZE + tx;

  float tmp = 0.0;
  for (int m=0; m < P; m=m+SIZE) {
    sA[ty][tx] = A[row*P + m + tx];
    sB[ty][tx] = B[col + (m + ty)*M];
    __syncthreads();
    for (int k=0; k<SIZE; k++)
      tmp += sA[ty][k] * sB[k][tx];
    __syncthreads();
  }
  C[row*M+col] = tmp;
}



void InitM(int N, int M, float *Mat);
int TestMM(int N, int M, int P, float *A, float *B, float *C);

int nTest = 0;

// How to run it:
// ./executable TAM test
// TAM is the matrix dimension
// test == 'Y', check results
// test == 'N', NO check results
// By default, N = 2048, test == 'N'

int main(int argc, char** argv)
{
  unsigned int N;
  unsigned int numBytesC, numBytesA, numBytesB;
  unsigned int nBlocks, nThreads;
 
  float TiempoTotal, TiempoKernel;
  cudaEvent_t E0, E1, E2, E3, XX;

  float *hA0, *hA1, *hB0, *hB1, *hC00, *hC01, *hC10, *hC11;
  float *dA0a, *dA1a, *dB0a, *dB1a, *dC00, *dC01, *dC10, *dC11;
  float *dA0b, *dA1b, *dB0b, *dB1b;

  // Matrix Picture 
  // Matrix A
      // First block of rows (N/2 x N)  : hA0
      // Second block of rows (N/2 x N) : hA1
  // Matrix B
      // First block of columns (N x N/2)  : hB0
      // Second block of columns (N x N/2) : hB1
  // Matrix C
      // Blocks of (N/2 x N/2) 
      // C00  |  C01
      // -----------
      // C10  |  C11


  // Based on the figure of the lab document, the device mapping is:
  // Device 0
  // C00 <- dA0a (hA0) x dB0a (hB0)
  // Device 1
  // C01 <- dA0a (hA0) x dB1a (hB1)
  // Device 2
  // C10 <- dA1a (hA1) x dB0b (hB0)
  // Device 3
  // C11 <- dA1a (hA1) x dB1b (hB1)

  int count;

  char test;

  // Matrix dimension and check result arguments 
  if (argc == 1)      { test = 'N'; N = 2048; }
  else if (argc == 2) { test = 'N'; N = atoi(argv[1]); }
  else if (argc == 3) { test = *argv[2]; N = atoi(argv[1]); }
  else { printf("Usage: ./exe TAM test\n"); exit(0); }

  // Number of threads in each dimension
  nThreads = SIZE;

  // Number of blocks in each dimension
  nBlocks = N/(2*nThreads); 
  // 2x2 GPUs, so, divide the total number of blocks by 2 in each grid dimension per GPU
  
  numBytesC = N * N * sizeof(float) / 4;
  numBytesA = N * N * sizeof(float) / 2;
  numBytesB = N * N * sizeof(float) / 2;

  dim3 dimGrid(nBlocks, nBlocks, 1);
  dim3 dimBlock(nThreads, nThreads, 1);


  cudaGetDeviceCount(&count);

  if (count < 4) { printf("No hay suficientes GPUs\n"); exit(0); }

  // Obtain host memory [pinned] 
  cudaMallocHost((float**)&hA0,  numBytesA); 
  cudaMallocHost((float**)&hA1,  numBytesA); 
  cudaMallocHost((float**)&hB0,  numBytesB); 
  cudaMallocHost((float**)&hB1,  numBytesB); 
  cudaMallocHost((float**)&hC00, numBytesC); 
  cudaMallocHost((float**)&hC01, numBytesC); 
  cudaMallocHost((float**)&hC10, numBytesC); 
  cudaMallocHost((float**)&hC11, numBytesC); 

  // Init matrices
  InitM(N/2, N, hA0);
  InitM(N/2, N, hA1);
  InitM(N, N/2, hB0);
  InitM(N, N/2, hB1);


  // Obtain the memory of each device
  cudaSetDevice(0);
  cudaMalloc((float**)&dA0a, numBytesA); 
  cudaMalloc((float**)&dB0a, numBytesB); 
  cudaMalloc((float**)&dC00, numBytesC); 

  cudaSetDevice(1);
  // TODO malloc

  cudaSetDevice(2);
  // TODO malloc

  cudaSetDevice(3);
  // TODO malloc

  cudaSetDevice(0);
  cudaEventCreate(&E0);
  cudaEventCreate(&E1);
  cudaEventCreate(&E2);
  cudaEventCreate(&E3);
  cudaEventCreate(&XX);

  cudaEventRecord(E0, 0);
  cudaEventSynchronize(E0);

  // Copy data from host to device
  cudaMemcpyAsync(dA0a, hA0, numBytesA, cudaMemcpyHostToDevice);
  cudaMemcpyAsync(dB0a, hB0, numBytesB, cudaMemcpyHostToDevice);
  cudaEventRecord(E1, 0); cudaEventSynchronize(E1);
  // Kernel execution
  KernelMM<<<dimGrid, dimBlock>>>(N/2, N/2, N, dA0a, dB0a, dC00);
  cudaEventRecord(E2, 0); cudaEventSynchronize(E2);
  // Obtain the rsult from device
  cudaMemcpyAsync(hC00, dC00, numBytesC, cudaMemcpyDeviceToHost); 
  cudaEventRecord(XX, 0);

  cudaSetDevice(1);
  // TODO copy to / execute /copy back
  cudaSetDevice(2);
  // TODO copy to / execute /copy back
  cudaSetDevice(3);
  // TODO copy to / execute /copy back

  cudaSetDevice(1); cudaEventSynchronize(XX);
  cudaSetDevice(2); cudaEventSynchronize(XX);
  cudaSetDevice(3); cudaEventSynchronize(XX);

  cudaSetDevice(0);
  cudaEventRecord(E3, 0);
  cudaEventSynchronize(E3);

  // Liberar Memoria del device 
  cudaSetDevice(0); cudaFree(dA0a); cudaFree(dB0a); cudaFree(dC00); 
  // TODO Free

  cudaEventElapsedTime(&TiempoTotal,  E0, E3);
  cudaEventElapsedTime(&TiempoKernel, E1, E2);
  printf("\nKERNEL MultiGPU - Matrix Product\n");
  printf("Dimensions: %dx%d\n", N, N);
  printf("nThreads: %dx%d (%d)\n", nThreads, nThreads, nThreads * nThreads);
  printf("nBlocks: %dx%d (%d)\n", nBlocks, nBlocks, nBlocks*nBlocks);
  printf("Pinned Memory Used\n");
  printf("Global Time: %4.6f milseg\n", TiempoTotal);
  printf("1 Kernel Time: %4.6f milseg\n", TiempoKernel);
  printf("Global Performance: %4.2f GFLOPS\n", (2.0 * (float) N * (float) N * (float) N) / (1000000.0 * TiempoTotal));
  printf("1 Kernel Performance:  %4.2f GFLOPS\n", (0.5 * (float) N * (float) N * (float) N) / (1000000.0 * TiempoKernel));
  printf("4 Kernels Performance: %4.2f GFLOPS\n", (2.0 * (float) N * (float) N * (float) N) / (1000000.0 * TiempoKernel));

  cudaSetDevice(0); cudaEventDestroy(E0); cudaEventDestroy(E1); cudaEventDestroy(E2); cudaEventDestroy(E3); cudaEventDestroy(XX);

  // El test will not work until you complete the algorithm
  if (test == 'N')
    printf ("NO TEST\n");
  else  if (TestMM(N/2, N/2, N, hA0, hB0, hC00) && 
            TestMM(N/2, N/2, N, hA0, hB1, hC01) && 
            TestMM(N/2, N/2, N, hA1, hB0, hC10) &&
            TestMM(N/2, N/2, N, hA1, hB1, hC11)) 
    printf ("TEST PASS\n");
  else
    printf ("TEST FAIL\n");

  cudaFreeHost(hA0); cudaFreeHost(hA1); 
  cudaFreeHost(hB0); cudaFreeHost(hB1); 
  cudaFreeHost(hC00); cudaFreeHost(hC01); cudaFreeHost(hC10); cudaFreeHost(hC11);

}


void InitM(int N, int M, float *Mat) {
   int i;
   for (i=0; i<N*M; i++) 
     Mat[i] = rand() / (float) RAND_MAX;
   
}

int error(float a, float b) {
  float tmp;

  tmp = abs(a-b) / abs(min(a,b));

  if (tmp > 0.0001) return 1;
  else  return 0;

}

int TestMM(int N, int M, int P, float *A, float *B, float *C) {
   int i, j, k;
   float tmp;
   printf("Pass %d\n", nTest); nTest++;
   for (i=0; i<N; i++)
     for (j=0; j<M; j++) {
       tmp = 0.0;
       for (k=0; k<P; k++) 
         tmp = tmp + A[i*P+k] * B[k*M+j]; 
       if (error(tmp, C[i*M+j])) {
         printf ("%d:%d: %f - %f = %f \n", i, j, tmp, C[i*M+j], abs(tmp - C[i*M+j]));
         return 0;
       }
     }
   
   return 1;
}

