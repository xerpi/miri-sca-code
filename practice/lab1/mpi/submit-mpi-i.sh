#!/bin/sh
# following option makes sure the job will run in the current directory
#$ -cwd
# following option makes sure the job has the same environmnent variables as the submission shell
#$ -V

setenv PROGRAM pi_mpi_i
make $PROGRAM

setenv size 1073741824
setenv MPI_PROCESSES 2
setenv OMP_NUM_THREADS 1

mpirun.mpich -np ${MPI_PROCESSES}  -machinefile $TMPDIR/machines ./trace.sh ./$PROGRAM $size


mpi2prv -f TRACE.mpits -o ${PROGRAM}_${size}_${MPI_PROCESSES}.prv
rm -rf TRACE.sym TRACE.spawn TRACE.mpits set-0

