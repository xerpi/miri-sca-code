#!/bin/bash
### Directivas para el gestor de colas
# Asegurar que el job se ejecuta en el directorio actual
#$ -cwd
# Asegurar que el job mantiene las variables de entorno del shell lamador
#$ -V
# Cambiar el nombre del job
#$ -N pi_cuda_trace
# Cambiar el shell
#$ -S /bin/bash

export CUDAVER=9.0.176

export PROG=pi_cuda.exe
export trials_per_block=32768
export blocks=256
export threads=256

echo "Trials per thread: 32768 Blocks: 256 Threads: 256" 1>&2
echo "$CUDAVER" 1>&2
export PATH=/Soft/cuda/$CUDAVER/bin:$PATH
/Soft/cuda/$CUDAVER/bin/nvprof --print-gpu-trace --unified-memory-profiling off ./$PROG $trials_per_block $threads $blocks


echo "Trials per thread: 32768 Blocks: 64 Threads: 1024" 1>&2
export blocks=64
export threads=1024
echo "$CUDAVER" 1>&2
/Soft/cuda/$CUDAVER/bin/nvprof --print-gpu-trace --unified-memory-profiling off ./$PROG $trials_per_block $threads $blocks


export trials_per_block=8192
export blocks=256
export threads=1024
echo "Trials per thread: $trials_per_block Blocks: $blocks Threads: $threads" 1>&2
/Soft/cuda/$CUDAVER/bin/nvprof --print-gpu-trace --unified-memory-profiling off ./$PROG $trials_per_block $threads $blocks


export trials_per_block=2048
export blocks=1024
export threads=1024
echo "Trials per thread: $trials_per_block Blocks: $blocks Threads: $threads" 1>&2
/Soft/cuda/$CUDAVER/bin/nvprof --print-gpu-trace --unified-memory-profiling off ./$PROG $trials_per_block $threads $blocks

export trials_per_block=262144
export blocks=256
export threads=32
echo "Trials per thread: $trials_per_block Blocks: $blocks Threads: $threads" 1>&2
/Soft/cuda/$CUDAVER/bin/nvprof --print-gpu-trace --unified-memory-profiling off ./$PROG $trials_per_block $threads $blocks

