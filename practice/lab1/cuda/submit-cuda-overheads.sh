#!/bin/bash
### Directivas para el gestor de colas
# Asegurar que el job se ejecuta en el directorio actual
#$ -cwd
# Asegurar que el job mantiene las variables de entorno del shell lamador
#$ -V
# Cambiar el nombre del job
#$ -N pi_cuda_api_trace
# Cambiar el shell
#$ -S /bin/bash


export PROG=pi_cuda.exe
export trials_per_block=32
export blocks=256
export threads=256

export CUDAVER=9.0.176
export PATH=/Soft/cuda/$CUDAVER/bin:$PATH

echo "$CUDAVER : Trace api"
/Soft/cuda/$CUDAVER/bin/nvprof --print-api-trace --unified-memory-profiling off ./$PROG $trials_per_block $threads $blocks

echo "$CUDAVER : Trace gpu"
/Soft/cuda/$CUDAVER/bin/nvprof --print-gpu-trace --unified-memory-profiling off ./$PROG $trials_per_block $threads $blocks

echo "$CUDAVER : Trace profiling with cpu"
/Soft/cuda/$CUDAVER/bin/nvprof --cpu-profiling on --unified-memory-profiling off ./$PROG $trials_per_block $threads $blocks

echo "$CUDAVER : No profile no trace"
./$PROG $trials_per_block $threads $blocks
