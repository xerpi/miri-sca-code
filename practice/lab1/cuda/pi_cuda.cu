//Derived somewhat from code developed by Patrick Rogers, UNC-C

#include <stdlib.h>
#include <stdio.h>
#include <cuda.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <curand_kernel.h>


#define TRIALS_PER_THREAD 32768ULL
#define BLOCKS  256ULL
#define THREADS 256ULL

#define PI 3.1415926535  // known value of pi

double getusec_() {
        struct timeval time;
        gettimeofday(&time, NULL);
        return ((double)time.tv_sec * (double)1e6 + (double)time.tv_usec);
}


#define START_COUNT_TIME stamp = getusec_();
#define STOP_COUNT_TIME(_m) stamp = getusec_() - stamp;\
                        stamp = stamp/1e6;\
                        printf ("%s%0.6fs\n",(_m), stamp);


__device__ float my_rand(unsigned long long int *seed) { 
       unsigned long long int a = 16807;  // constants for random number generator 
       unsigned long long int m = 2147483647;   // 2^31 - 1 
       unsigned long long int x = (unsigned long) *seed; 
       x = (a * x)%m; 
       *seed = (unsigned long long int) x; 
       return ((float)x)/m; 
} 

__attribute__ ((noinline)) unsigned long long int foo(unsigned long long int trials_per_thread)
{
       volatile unsigned long long int k;
       for (k=0;k<trials_per_thread;k++);

       return k;
}


__global__ void gpu_monte_carlo(float *estimate, unsigned long long int trials_per_thread) { 
       unsigned int tid = threadIdx.x + blockDim.x * blockIdx.x; 
       int points_in_circle = 0; 
       float x, y; 
       unsigned long long int seed =  tid + 1;  // starting number in random sequence 
       for(unsigned int i = 0; i < trials_per_thread ; i++) { 
              x = my_rand(&seed); 
              y = my_rand(&seed); 
              points_in_circle += (x*x + y*y <= 1.0f); // count if x & y is in the circle. 
       } 
       estimate[tid] = 4.0f * points_in_circle / (float) trials_per_thread; 
} 

void CheckCudaError(char sms[]) {
  cudaError_t error;
 
  error = cudaGetLastError();
  if (error) printf("ERROR. %s: %s\n", sms, cudaGetErrorString(error));
}


int main (int argc, char *argv[]) { 
       double stamp;
       //clock_t start, stop; 
       float *dev; 

       const char Usage[] = "Usage: pi <trials_per_thread> <threads> <blocks> (try 32768 256 256)\n";
       if (argc < 2) {
	fprintf(stderr, Usage);
	exit(1);
       }

       unsigned long long int trials_per_thread = atoll(argv[1]);
       unsigned long long int threads= atoll(argv[2]);
       unsigned long long int blocks= atoll(argv[3]);
       float *host = (float *)malloc(blocks*threads*sizeof(float));
       cudaDeviceReset();
       CheckCudaError((char *) "Device Reset"); 

       cudaFree(NULL);
       CheckCudaError((char *) "Cuda Free"); 

       START_COUNT_TIME;
       cudaMalloc((void **) &dev, blocks * threads * sizeof(float));  
       CheckCudaError((char *) "Cuda Malloc"); 
#ifdef _DEBUG_
       STOP_COUNT_TIME("Cuda Malloc Elapsed Time: ");
#endif

#ifdef _DEBUG_
       START_COUNT_TIME;
#endif
       gpu_monte_carlo<<<blocks, threads>>>(dev, trials_per_thread); 
       CheckCudaError((char *) "kernel execution"); 
#ifdef _DEBUG_
       STOP_COUNT_TIME("Kernel Execution Elapsed Time: ");
#endif

#ifdef _DEBUG_
       START_COUNT_TIME;
#endif
       cudaMemcpy(host, dev, blocks * threads * sizeof(float), cudaMemcpyDeviceToHost);  
       CheckCudaError((char *) "Cuda Mem copy"); 
#ifdef _DEBUG_
       STOP_COUNT_TIME("Cuda Memcpy Elapsed Time: ");
#endif

#ifdef _DEBUG_
       START_COUNT_TIME;
#endif
       float pi_gpu=0; 
       for(int i = 0; i < blocks * threads; i++) { 
        pi_gpu += host[i]; 
       } 
       pi_gpu /= (blocks* threads); 
#ifdef _DEBUG_
       STOP_COUNT_TIME("Host reduction Elapsed Time: ");
#endif

#ifdef _DEBUG_
       START_COUNT_TIME;
#endif
       printf("Number pi after %lld iterations = %.15f\n", blocks * threads * trials_per_thread, pi_gpu);
#ifdef _DEBUG_
       STOP_COUNT_TIME("Printf Elapsed Time: ");
#else
       STOP_COUNT_TIME("Total Elapsed Time: ");
#endif

       volatile unsigned long long int k = foo(blocks*threads*trials_per_thread);
       //volatile unsigned long long int k = blocks*threads*trials_per_thread;
       printf("Total number of trials: %llu\n",k);

       cudaDeviceReset();
       CheckCudaError((char *) "Device Reset"); 
 return 0; 
} 
