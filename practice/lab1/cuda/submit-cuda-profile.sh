#!/bin/bash
### Directivas para el gestor de colas
# Asegurar que el job se ejecuta en el directorio actual
#$ -cwd
# Asegurar que el job mantiene las variables de entorno del shell lamador
#$ -V
# Cambiar el nombre del job
#$ -N pi_cuda_profile
# Cambiar el shell
#$ -S /bin/bash


export PROG=pi_cuda.exe
export trials_per_block=32768
export blocks=256
export threads=256


#Some undefined sybmols appear with newer version of the nvprof. 
#This is the reason we are using this version to show all the symbols:
export CUDAVER=7.5.18
export PATH=/Soft/cuda/$CUDAVER/bin:$PATH

/Soft/cuda/$CUDAVER/bin/nvprof --unified-memory-profiling off --cpu-profiling on ./$PROG $trials_per_block $threads $blocks

#This another execution with an newer profiling helps us to show the time in the CPU, not shown in previous profiling
export CUDAVER=9.0.176
export PATH=/Soft/cuda/$CUDAVER/bin:$PATH
/Soft/cuda/$CUDAVER/bin/nvprof --unified-memory-profiling off --cpu-profiling on ./$PROG $trials_per_block $threads $blocks
