#!/bin/csh
# following option makes sure the job will run in the current directory
#$ -cwd
# following option makes sure the job has the same environmnent variables as the submission shell
#$ -V

setenv PROGRAM pi_mpi_omp_i
make $PROGRAM



setenv size 1073741824

foreach mpi_i ( 1 2 3 )
  setenv MPI_PROCESSES $mpi_i
  foreach omp_i ( 1 2 3 4 6 8 12 24 )
    setenv OMP_NUM_THREADS $omp_i 
    echo "Executing... mpi $mpi_i omp $omp_i"
    mpirun.mpich -np ${MPI_PROCESSES}  -machinefile $TMPDIR/machines ./trace.sh ./$PROGRAM $size
    ${EXTRAE_HOME}/bin/mpi2prv -f TRACE.mpits -o ${PROGRAM}_${size}_${MPI_PROCESSES}_${OMP_NUM_THREADS}.prv
    rm -rf TRACE.sym TRACE.spawn TRACE.mpits set-0
  end 
end
