# Read the header line of each paraver trace in order to obtain the execution time (in ns) that took the trace. This is done for all the combinations.

echo 
echo "node 1"
echo 
  head -n 1 pi_mpi_omp_i_1073741824_1_1.prv pi_mpi_omp_i_1073741824_1_2.prv pi_mpi_omp_i_1073741824_1_3.prv pi_mpi_omp_i_1073741824_1_4.prv pi_mpi_omp_i_1073741824_1_6.prv pi_mpi_omp_i_1073741824_1_8.prv pi_mpi_omp_i_1073741824_1_12.prv pi_mpi_omp_i_1073741824_1_24.prv | cut -d ":" -f 3 | cut -d "_" -f 1 | grep -v "pi"
echo 
echo "node 2"
echo 
  head -n 1 pi_mpi_omp_i_1073741824_2_1.prv pi_mpi_omp_i_1073741824_2_2.prv pi_mpi_omp_i_1073741824_2_3.prv pi_mpi_omp_i_1073741824_2_4.prv pi_mpi_omp_i_1073741824_2_6.prv pi_mpi_omp_i_1073741824_2_8.prv pi_mpi_omp_i_1073741824_2_12.prv pi_mpi_omp_i_1073741824_1_24.prv | cut -d ":" -f 3 | cut -d "_" -f 1 | grep -v "pi"

echo 
echo "node 3"
echo 

  head -n 1 pi_mpi_omp_i_1073741824_3_1.prv pi_mpi_omp_i_1073741824_3_2.prv pi_mpi_omp_i_1073741824_3_3.prv pi_mpi_omp_i_1073741824_3_4.prv pi_mpi_omp_i_1073741824_3_6.prv pi_mpi_omp_i_1073741824_3_8.prv pi_mpi_omp_i_1073741824_3_12.prv pi_mpi_omp_i_1073741824_1_24.prv | cut -d ":" -f 3 | cut -d "_" -f 1 | grep -v "pi"

