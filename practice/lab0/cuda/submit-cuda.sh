#!/bin/bash
### Directivas para el gestor de colas
# Asegurar que el job se ejecuta en el directorio actual
#$ -cwd
# Asegurar que el job mantiene las variables de entorno del shell lamador
#$ -V
# Cambiar el nombre del job
#$ -N pi_cuda
# Cambiar el shell
#$ -S /bin/bash


# trials_per_thread=32768
export PROG=pi_cuda_events.exe
#export PROG=pi_cuda.exe
export blocks=256
export threads=256

/usr/bin/time ./$PROG $threads $blocks
