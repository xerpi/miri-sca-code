//Derived somewhat from code developed by Patrick Rogers, UNC-C

#include <stdlib.h>
#include <stdio.h>
#include <cuda.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <curand_kernel.h>


#define TRIALS_PER_THREAD 32768ULL
#define BLOCKS  256ULL
#define THREADS 256ULL

#define PI 3.1415926535  // known value of pi

double getusec_() {
        struct timeval time;
        gettimeofday(&time, NULL);
        return ((double)time.tv_sec * (double)1e6 + (double)time.tv_usec);
}


#define START_COUNT_TIME stamp = getusec_();
#define STOP_COUNT_TIME(_m) stamp = getusec_() - stamp;\
                        stamp = stamp/1e6;\
                        printf ("%s%0.6fs\n",(_m), stamp);


__device__ float my_rand(unsigned long long int *seed) { 
       unsigned long long int a = 16807;  // constants for random number generator 
       unsigned long long int m = 2147483647;   // 2^31 - 1 
       unsigned long long int x = (unsigned long) *seed; 
       x = (a * x)%m; 
       *seed = (unsigned long long int) x; 
       return ((float)x)/m; 
} 


__global__ void gpu_monte_carlo(float *estimate) { 
       unsigned int tid = threadIdx.x + blockDim.x * blockIdx.x; 
       int points_in_circle = 0; 
       float x, y; 
       unsigned long long int seed =  tid + 1;  // starting number in random sequence 
       for(unsigned int i = 0; i < TRIALS_PER_THREAD; i++) { 
              x = my_rand(&seed); 
              y = my_rand(&seed); 
              points_in_circle += (x*x + y*y <= 1.0f); // count if x & y is in the circle. 
       } 
       estimate[tid] = 4.0f * points_in_circle / (float) TRIALS_PER_THREAD; 
} 


int main (int argc, char *argv[]) { 
       double stamp;
       //clock_t start, stop; 
       float *dev; 

       const char Usage[] = "Usage: pi <threads> <blocks> (trials per thread fixed to 32768 - try 256 256)\n";
       if (argc < 2) {
	fprintf(stderr, Usage);
	exit(1);
       }

       unsigned long long int threads= atoll(argv[1]);
       unsigned long long int blocks= atoll(argv[2]);
       float *host = (float *)malloc(blocks*threads*sizeof(float));

       cudaFree(0);
       START_COUNT_TIME;
       cudaMalloc((void **) &dev, blocks * threads * sizeof(float));  
#ifdef _DEBUG_
       STOP_COUNT_TIME("Cuda Malloc Elapsed Time: ");
#endif

#ifdef _DEBUG_
       START_COUNT_TIME;
#endif
       gpu_monte_carlo<<<blocks, threads>>>(dev); 
#ifdef _DEBUG_
       STOP_COUNT_TIME("Kernel Execution Elapsed Time: ");
#endif

#ifdef _DEBUG_
       START_COUNT_TIME;
#endif
       cudaMemcpy(host, dev, blocks * threads * sizeof(float), cudaMemcpyDeviceToHost);  
#ifdef _DEBUG_
       STOP_COUNT_TIME("Cuda Memcpy Elapsed Time: ");
#endif

#ifdef _DEBUG_
       START_COUNT_TIME;
#endif
       float pi_gpu=0.0; 
       for(int i = 0; i < blocks * threads; i++) { 
        pi_gpu += host[i]; 
       } 
       pi_gpu /= (blocks* threads); 
#ifdef _DEBUG_
       STOP_COUNT_TIME("Host reduction Elapsed Time: ");
#endif

#ifdef _DEBUG_
       START_COUNT_TIME;
#endif
       printf("Number pi after %lld iterations = %.15f\n", blocks * threads * TRIALS_PER_THREAD, pi_gpu);
#ifdef _DEBUG_
       STOP_COUNT_TIME("Printf Elapsed Time: ");
#else
       STOP_COUNT_TIME("Total Elapsed Time: ");
#endif

 return 0; 
} 
