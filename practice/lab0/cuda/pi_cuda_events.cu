//Derived somewhat from code developed by Patrick Rogers, UNC-C

#include <stdlib.h>
#include <stdio.h>
#include <cuda.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <curand_kernel.h>


#define TRIALS_PER_THREAD 32768ULL
#define BLOCKS  256ULL
#define THREADS 256ULL

#define PI 3.1415926535  // known value of pi


__device__ float my_rand(unsigned long long int *seed) { 
       unsigned long long int a = 16807;  // constants for random number generator 
       unsigned long long int m = 2147483647;   // 2^31 - 1 
       unsigned long long int x = (unsigned long) *seed; 
       x = (a * x)%m; 
       *seed = (unsigned long long int) x; 
       return ((float)x)/m; 
} 


__global__ void gpu_monte_carlo(float *estimate) { 
       unsigned int tid = threadIdx.x + blockDim.x * blockIdx.x; 
       int points_in_circle = 0; 
       float x, y; 
       unsigned long long int seed =  tid + 1;  // starting number in random sequence 
       for(unsigned int i = 0; i < TRIALS_PER_THREAD; i++) { 
              x = my_rand(&seed); 
              y = my_rand(&seed); 
              points_in_circle += (x*x + y*y <= 1.0f); // count if x & y is in the circle. 
       } 
       estimate[tid] = 4.0f * points_in_circle / (float) TRIALS_PER_THREAD; 
} 


int main (int argc, char *argv[]) { 
       float *dev; 

       const char Usage[] = "Usage: pi <trials_per_thread> <threads> <blocks> (try 32768 256 256)\n";
       if (argc < 2) {
	fprintf(stderr, Usage);
	exit(1);
       }

       float elapsed_time;
       cudaEvent_t E0;
       cudaEvent_t E1;
       cudaEvent_t E2;
       cudaEvent_t E3;
       cudaEvent_t E4;


       unsigned long long int threads= atoll(argv[1]);
       unsigned long long int blocks= atoll(argv[2]);
       float *host = (float *)malloc(blocks*threads*sizeof(float));

       cudaEventCreate(&E0);
       cudaEventCreate(&E1);
       cudaEventCreate(&E2);
       cudaEventCreate(&E3);
       cudaEventCreate(&E4);

       cudaEventRecord(E0,0); cudaEventSynchronize(E0);

       cudaMalloc((void **) &dev, blocks * threads * sizeof(float));  

       cudaEventRecord(E1,0); cudaEventSynchronize(E1);

       gpu_monte_carlo<<<blocks, threads>>>(dev); 

       cudaEventRecord(E2,0); cudaEventSynchronize(E2);

       cudaMemcpy(host, dev, blocks * threads * sizeof(float), cudaMemcpyDeviceToHost);  

       cudaEventRecord(E3,0); cudaEventSynchronize(E3);

       float pi_gpu=0.0; 
       for(int i = 0; i < blocks * threads; i++) { 
        pi_gpu += host[i]; 
       } 
       pi_gpu /= (blocks* threads); 

       cudaEventRecord(E4,0); cudaEventSynchronize(E4);


       cudaEventElapsedTime(&elapsed_time, E0, E1);
       printf("Cuda Malloc Elapsed Time: %.6fs\n",elapsed_time/1000.0);
       cudaEventElapsedTime(&elapsed_time, E1, E2);
       printf("Kernel Execution Elapsed Time: %.6fs\n",elapsed_time/1000.0);
       cudaEventElapsedTime(&elapsed_time, E2, E3);
       printf("Cuda Memcpy Elapsed Time: %.6fs\n",elapsed_time/1000.0);
       cudaEventElapsedTime(&elapsed_time, E3, E4);
       printf("Host reduction Elapsed Time: %.6fs\n",elapsed_time/1000.0);

       printf("Number pi after %lld iterations = %.15f\n", blocks * threads * TRIALS_PER_THREAD, pi_gpu);

       cudaEventDestroy(E0);
       cudaEventDestroy(E1);
       cudaEventDestroy(E2);
       cudaEventDestroy(E3);
       cudaEventDestroy(E4);

 return 0; 
} 
