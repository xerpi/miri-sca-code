#!/bin/bash
# following option makes sure the job will run in the current directory
#$ -cwd
# following option makes sure the job has the same environmnent variables as the submission shell
#$ -V
# Per canviar de shell
#$ -S /bin/bash

PROGRAM=pi_mpi_omp
size=1073741824

for i in `seq 1 12`;
do
    echo "Number of threads:" $i
    export OMP_NUM_THREADS=$i
    mpirun.mpich -np 2 -machinefile $TMPDIR/machines ./$PROGRAM $size
done   
