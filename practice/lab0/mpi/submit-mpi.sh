#!/bin/sh
# following option makes sure the job will run in the current directory
#$ -cwd
# following option makes sure the job has the same environmnent variables as the submission shell
#$ -V

setenv PROGRAM pi_mpi
setenv size 1073741824

mpirun.mpich -np 3 -machinefile $TMPDIR/machines ./$PROGRAM $size

