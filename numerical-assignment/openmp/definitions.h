#ifndef DEFINITIONS_H
#define DEFINITIONS_h

#if _EXTRAE_

#include "extrae_user_events.h"
// Extrae Constants
#define  PROGRAM    1000
#define  END        0

#define SERIAL		1
#define INIT_MATRICES	2
#define SETUP_RHS1	3
#define PCG		4
#define CHECK_ANSWER	5

#define MUL_POISSON     6

//#define  PARALLEL   2
#endif

#endif