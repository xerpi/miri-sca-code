#!/bin/bash

PROGRAM=cgp3d_seq_gprof

size=100
preconds=(id as ssor)
max_it=350

make $PROGRAM

for precond in "${preconds[@]}"
do
	echo "Preconditioner: $precond, size: $size, max it: $max_it"

	./$PROGRAM -p $precond -n $size -M $max_it

	gprof $PROGRAM
done