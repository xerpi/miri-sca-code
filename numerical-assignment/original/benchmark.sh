#!/bin/bash

sizes=(100 120 140 160)
preconds=(id as ssor)
max_it=350

for precond in "${preconds[@]}"
do
	for size in "${sizes[@]}"
	do
		export PROGRAM=cgp3d.x
		export PROG_ARGS="-p $precond -n $size -M $max_it"
	
		make $PROGRAM
	
		echo "Preconditioner: $precond, size: $size, max it: $max_it"
	
		qsub -sync yes -l execution benchmark-submit.sh
	done
done