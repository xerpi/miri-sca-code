#ifndef TIMING_H
#define TIMING_H

#include <unistd.h>
#include <sys/time.h>

#define NWATCHES 8
#define CLOCK CLOCK_MONOTONIC

void   tic(int watch);
double toc(int watch);

#endif /* TIMING_H */
