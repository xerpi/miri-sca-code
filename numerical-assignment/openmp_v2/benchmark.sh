#!/bin/bash

program=cgp3d_omp

#sizes=(100 120 140 160)
#preconds=(id as ssor)
sizes=(200)
preconds=(id)
nthreads=(1 4 8 12 16 20 24)
max_it=500

make "$program"

for precond in "${preconds[@]}"
do
	for size in "${sizes[@]}"
	do
		for nthread in "${nthreads[@]}"
		do
			export PROGRAM="$program"
			export PROG_ARGS="-p $precond -n $size -M $max_it"
			export NUM_THREADS="$nthread"
		
			echo "#Threads: $nthread, preconditioner: $precond, size: $size, max it: $max_it"
		
			qsub -sync yes -l execution benchmark-submit.sh
		done
	done
done