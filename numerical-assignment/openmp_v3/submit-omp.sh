#!/bin/csh
# following option makes sure the job will run in the current directory
#$ -cwd
# following option makes sure the job has the same environmnent variables as the submission shell
#$ -V

setenv PROG cgp3d_omp
make $PROG

setenv OMP_NUM_THREADS 8

./$PROG -n 160 -p id -M 350
