#!/bin/csh
# following option makes sure the job will run in the current directory
#$ -cwd
# following option makes sure the job has the same environmnent variables as the submission shell
#$ -V

echo "${NUM_THREADS} | ${PROGRAM} ${PROG_ARGS}"

setenv PROG ${PROGRAM}
setenv OMP_NUM_THREADS ${NUM_THREADS}

./${PROGRAM} ${PROG_ARGS}
