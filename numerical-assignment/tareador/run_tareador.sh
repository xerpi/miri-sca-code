USAGE="\n USAGE: ./run_tareador.sh prog args \n
        prog        -> Tareador program name\n"

if (test $# -lt 1 || test $# -gt 7)
then
        echo -e $USAGE
        exit 0
fi

make $1

rm -rf .tareador_precomputed_*
tareador_gui.py --llvm --lite -b "$2 $3 $4 $5 $6 $7" $1
