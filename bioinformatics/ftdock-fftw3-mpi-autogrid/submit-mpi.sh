#!/bin/bash

mkdir -p data
rm -f data/*
for i in 1 2 4 8 12 16 20 24
do
	echo -n "" > tmp.sh
	cat > tmp.sh <<EOF
#!/bin/csh
#$ -cwd
#$ -V

echo "-> Executing with ${i} MPI ranks"
echo -e "\t+ 2PKA"
mpirun.mpich -np $i ./ftdock-sca -noelec -static ../proteins/2pka.parsed -mobile ../proteins/5pti.parsed > data/output_2pka_${i}r.txt
echo -e "\t+ 1HBA"
mpirun.mpich -np $i ./ftdock-sca -noelec -static ../proteins/1hba.parsed -mobile ../proteins/5pti.parsed > data/output_1hba_${i}r.txt
echo -e "\t+ 4HHB"
mpirun.mpich -np $i ./ftdock-sca -noelec -static ../proteins/4hhb.parsed -mobile ../proteins/5pti.parsed > data/output_4hhb_${i}r.txt
EOF
	echo "Submitting job with ${i} MPI ranks"
	qsub -sync yes -l cuda -q cuda.q tmp.sh
done
