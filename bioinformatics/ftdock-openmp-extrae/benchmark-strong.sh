#!/bin/bash

program=ftdock-sca

nthreads=(1 4 8 12 16 20 24)

statics=(2pka 1hba 4hhb)

make "$program"

for static in "${statics[@]}"
do
	for index in "${!nthreads[@]}"
	do
		nthread="${nthreads[$index]}"
		
		export PROGRAM="$program"
		export PROG_ARGS="-noelec -static ../proteins/$static.parsed -mobile ../proteins/5pti.parsed"
		export NUM_THREADS="$nthread"
	
		echo "#Threads: $nthread, static: $static"
	
		qsub -sync yes -l execution benchmark-submit.sh
	done
done
