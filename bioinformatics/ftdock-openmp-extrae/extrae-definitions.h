#ifndef DEFINITIONS_H
#define DEFINITIONS_h

#if _EXTRAE_

#include "extrae_user_events.h"
// Extrae Constants
#define  PROGRAM    1000
#define  END        0

#define SERIAL  	        1
#define LOOP			2
#define ROTATE_MOBILE		3
#define DISCRETISE_MOBILE	4
#define FORWARD_FFT		5
#define CONVOLUTION		6
#define REVERSE_FFT		7
#define GET_BEST_SCORES		8

#endif

#endif
