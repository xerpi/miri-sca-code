#!/bin/csh
#$ -cwd
#$ -V

mkdir -p data
./ftdock-sca -noelec -static ../proteins/2pka.parsed -mobile ../proteins/5pti.parsed > data/2pka.txt

./ftdock-sca -calculate_grid 1 -noelec -static ../proteins/2pka.parsed -mobile ../proteins/5pti.parsed > data/2pka_optgrid.txt
