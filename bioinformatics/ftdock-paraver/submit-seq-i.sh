#!/bin/csh
# following option makes sure the job will run in the current directory
#$ -cwd
# following option makes sure the job has the same environmnent variables as the submission shell
#$ -V

setenv PROG ftdock-sca_i
make $PROG

#setenv OMP_NUM_THREADS 1

#setenv LD_PRELOAD ${EXTRAE_HOME}/lib/libomptrace.so
setenv LD_PRELOAD

./$PROG -noelec -static ../proteins/2pka.parsed -mobile ../proteins/5pti.parsed

setenv LD_PRELOAD 
mpi2prv -f TRACE.mpits -o ${PROG}.prv -e $PROG -paraver
rm -rf TRACE.mpits TRACE.sym set-0 >& /dev/null
