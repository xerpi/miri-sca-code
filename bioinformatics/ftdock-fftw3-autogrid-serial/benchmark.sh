#!/bin/bash

program=ftdock-sca

statics=(2pka 1hba 4hhb)

make "$program"

for static in "${statics[@]}"
do
	export PROGRAM="$program"
	export PROG_ARGS="-noelec -static ../proteins/$static.parsed -mobile ../proteins/5pti.parsed"
	echo "static: $static"
	
	qsub -sync yes -l execution benchmark-submit.sh
done
