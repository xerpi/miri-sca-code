#!/bin/bash

mkdir -p data
for threads in 2 4 6 12
do
	mkdir -p data/${threads}
	for ranks in 1 2 4 6 12
	do
		coresRequired=$(( $threads * $ranks ))
		if [ $coresRequired -gt 24 ]
		then
			continue
		fi
		echo "Submitting with ${ranks} ranks and $threads threads"
		echo -n "" > tmp.sh
		cat > tmp.sh <<EOF
#!/bin/csh
#$ -cwd
#$ -V

setenv OMP_NUM_THREADS $threads

mpirun.mpich -np $ranks ./ftdock-sca -noelec -static ../proteins/2pka.parsed -mobile ../proteins/5pti.parsed > data/${threads}/output_2pka_${ranks}r.txt
mpirun.mpich -np $ranks ./ftdock-sca -noelec -static ../proteins/1hba.parsed -mobile ../proteins/5pti.parsed > data/${threads}/output_1hba_${ranks}r.txt
mpirun.mpich -np $ranks ./ftdock-sca -noelec -static ../proteins/4hhb.parsed -mobile ../proteins/5pti.parsed > data/${threads}/output_4hhb_${ranks}r.txt
EOF
	
		qsub -sync yes -l cuda -q cuda.q tmp.sh
	done
done
