#!/bin/csh
# following option makes sure the job will run in the current directory
#$ -cwd
# following option makes sure the job has the same environmnent variables as the submission shell
#$ -V

echo "${PROGRAM} ${PROG_ARGS}"

setenv PROG ${PROGRAM}

./${PROGRAM} ${PROG_ARGS}
